<?php
//Наследование абстрактного класса
class ShopTerminal extends CashMachine {
    //Реализация абстрактного метода
    public function login($pinCode)     {
        if ($pinCode === 1111) {
            $this->greeting();
        }
        else {
            echo 'Введен неправильный PIN!';
        }
    }
}
//Создаем экземпляр магазинного терминала
$shopTerminal = new ShopTerminal();
$shopTerminal->login(1111);
/* * Результат: * Вы успешно вошли в систему! */  	$shopTerminal->login(4567);
/* * Результат: * Введен неправильный PIN! */
