<?php
class User {
    //Cтатическое свойство количества пользователей
    static public $userCount = 0;
    //Обычное свойство порядкового номера юзера
    public $userId = null;
    //Новый пользователь: увеличиваем идентификатор
    public function __construct()     {
        $this->userId = ++self::$userCount;
    }      //При удалении пользователя уменьшаем счетчик
    public function __destruct()   {
        --self::$userCount;
    }      //Показ порядкового номера текущего пользователя
    public function getUserId()     {
        echo 'Номер пользователя - '. $this->userId .'<br>';
    }      //Показ общего числа существующих пользователей
    public function getCount()     {
        echo 'Общее количество - '. self::$userCount.'<br>';
    }
}
//Создаем пользователя и выводим показатели
$userFirst = new User();
$userFirst->getUserId();
//Результат: Номер пользователя – 1
$userFirst->getCount();
//Результат: Общее количество – 1
//Создаем еще двух пользователей
$userSecond = new User();
$userThird = new User();
$userThird->getUserId();
//Результат: Номер пользователя – 3
$userThird->getCount();
//Результат: Общее количество – 3
//Удаляем первого и второго пользователя 	unset($userSecond, $userFirst);
$userThird->getUserId();
//Результат: Номер пользователя – 3
$userThird->getCount();
//Результат: Общее количество - 1




