<?php
//Определяем класс "Cолдат"
class Soldier {
    //Свойства: имя, звание и номер жетона
    protected $solderName, $solderRank, $solderToken;
    //Создаем конструктор класса, который
    //принимает три параметра
    public function __construct($name, $rank, $token)     {
        $this->solderToken = $token;
        $this->solderName = $name;
        $this->solderRank = $rank;
    }
    //Метод вывода информации в браузер
    public function reportStatus()     {
        echo "<h4>Статус солдата</h4>";
        echo "Имя: {$this->solderName} <br>";
        echo "Звание: {$this->solderRank} <br>";
        echo "Жетон: {$this->solderToken}<br>";
    }
}  		//Создаем экземпляр класса
$Solder = new Soldier('Иван', 'Рядовой', 1500);
$Solder->reportStatus();
/* * Результат:
* Статус солдата
* Имя: Иван
* Звание: Рядовой
* Жетон: 1500 */

