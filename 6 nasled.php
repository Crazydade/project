<?php
//Определяем класс с общим функционалом
class Dog {
    //свойство окраса шерсти
    public $hairColor;
    //свойство имени собаки
    public $dogName;
    //метод вывода информации
    public function showDogInfo()     {
        echo "Кличка собаки: {$this->dogName}<br>";
        echo "Цвет шерсти: {$this->hairColor}<br>";
    }
}
//Определяем класс большой собаки, который
//наследует класс обычной собаки
class BigDog extends Dog {
    //метод громкого гавканья
    public function loudBark()     {
        echo 'Гав! Гав! Гав...';
    }
}
//Создаем экземпляр большой собаки
$myDog = new BigDog();
$myDog->hairColor = 'дымчатый';
$myDog->dogName = 'шарик';
//Вызываем унаследованный метод
$myDog->showDogInfo();
/* * Результат:
* Кличка собаки: шарик  Цвет шерсти: дымчатый */  	//Вызываем собственный метод
$myDog->loudBark();
/* * Результат: * Гав! Гав! Гав... */
