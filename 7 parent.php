<?php
class Car {
    //Метод движения автомобиля вперед
    public function goAhead()     {
        echo 'Едем по прямой...  ';
    }
}
//Определяем класс "Спортивный автомобиль«
class SportCar extends Car {
    //Внутри метода вызываем родительский
    public function goAhead()     {
        parent::goAhead();
        echo 'Очень быстро!!!<br>';
    }
}
//Создаем экземпляр и вызываем метод
$sportCar = new SportCar();
$sportCar->goAhead();
/* * Результат:
* Едем по прямой...  Очень быстро!!! */
