<?php
//Интерфейс "Персонального компьютера”
interface PersonalComputer {
    //Определения метода включения
    public function workStart($userName);
}
//Класс "Ноутбук" реализующий интерфейс
class Notebook implements PersonalComputer {
    public function workStart($userName)     {
        echo "Добро пожаловать {$userName}";
    }
}
