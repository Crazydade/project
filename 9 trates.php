<?php
class Circle {
    use AreaCir, PerCir;
    //Задаем метод расчета и вывода на экран площади круга
    public function getArea($val)   {
        echo "Площадь круга = " ;
    }
    //Задаем метод расчета и вывода на экран окружности круга
    public function getPerimeter($val) {
        echo "Длина окружности круга = " ;
    }
}

trait AreaCir {
    public function getAreaCir($val){
        $area = pi()*$val*$val;
        echo "$area <BR>" ;
    }
}

trait PerCir{
    public function getPerCir($val)  {
        $perimeter = pi() * $val * 2;
        echo "$perimeter <BR>" ;
    }
}

$obj = new Circle();
$obj->getArea($val);
$obj->getAreaCir($val);
$obj->getPerimeter($val);
$obj->getPerCir($val);
